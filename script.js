
var app = new Vue({

    el: '#app',

    data: {
        filmData: new Array
    },

    methods: {
        filmDescription: function(index) {
            alert(this.filmData[index].description)
        }
    },

    mounted: function() {

        fetch('https://ghibliapi.herokuapp.com/films')

        .then((response) => {
            return response.json()
        })

        .then((filmData) => { 
            this.filmData = filmData
        })

        .catch((err) => {
            console.log(err)     
        })
    }
    
})
